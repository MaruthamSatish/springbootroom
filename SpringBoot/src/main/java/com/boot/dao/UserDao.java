package com.boot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.boot.model.User;
@Transactional
public interface UserDao extends CrudRepository<User, Long> {

}
